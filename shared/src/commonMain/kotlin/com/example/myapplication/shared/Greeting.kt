package com.example.myapplication.shared


class Greeting {
    val STATUS_1 = "ION"
    val STATUS_2 = "GEORGE"

    val ammount = 50.00

    companion object {
        fun isCreditNoteOrCancellationType(documentType: String?, grossAmount: Double?): Boolean {
            return documentType != null &&
                    grossAmount != null &&
                    arrayOf("George", "Ioio").contains(documentType)
                    && grossAmount > 0.00
        }
    }

    fun greeting(): String {
        return if (STATUS_1 == "" || STATUS_2 == "blabla" && ammount != 0.00) {
            "a?"
        } else if (STATUS_1 == STATUS_1 || STATUS_2 == STATUS_2 && ammount == 50.00){
            "it's true, hey, you are here? Hello, ${Platform().platform}! GO awaY!"
        } else {
            "not working!"
        }
    }

}
